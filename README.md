Se tiene la concesión del comedor de un colegio y se desea que los padres o responsables de los estudiantes puedan confeccionar la lista de comidas que cada uno de sus hijos puede comprar, facturar de acuerdo a dicha selección y consumo y tener información de utilidad para su negocio.

Una nutricionista define un conjunto de menús recomendados de una lista de comidas predefinida. Los padres una vez por semana pueden seleccionar los menús predefinidos (para cada día lectivo) o definirlos ellos mismos.
Asociado a cada comida hay un precio por lo que hay un monto mensual que los padres deberán abonar.

Cada comida/producto que ofrece el comedor tiene un conjunto de atributos: cárnico, lácteo, con gluten, con azúcar agregado, snack, dietético, primer plato, segundo plato, postre, bebida, etc (se pueden ingresar todos los que el usuario considere convenientes).

Requerimientos funcionales
1. Elsistemacuentaconcategoríasdeusuario(EstoimplicaABMdeusuariose implementación de Login al sistema) :
a. Administrador: accede a toda la aplicación, asigna el precio a cada comida/producto, factura/registra el pago mensual por parte de los padres.
b. Dietista: ingreso de comidas, atributos para las mismas y confección de menús.
c. Padre: selección de comidas/menús por día de la semana y selección de productos (según atributos) que su hijo puede consumir.
d. Vendedor: entrega al estudiante lo correspondiente del menú asignado para ese día. También puede venderle otros productos por fuera del menú siempre que estén permitidos por su padre.
2. El sistema debe permitir toda la funcionalidad descrita en el punto 1).
3. El sistema debe permitir las siguiente consultas y listados:
a. Factura a cobrar a cada padre para el mes actual con detalle de consumos.
b. Totales vendidos entre fechas, con subtotales por menú.