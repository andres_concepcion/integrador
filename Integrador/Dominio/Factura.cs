﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class Factura
    {
        private int id;
        private DateTime fecha;
        private Alumno alumno;
        private double precio;
        private List<Producto> lstProductos;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        public Alumno Alumno
        {
            get { return alumno; }
            set { alumno = value; }
        }

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }
        public List<Producto> LstProductos
        {
            get { return lstProductos; }
            set { lstProductos = value; }
        }

        public Factura()
        {
            lstProductos = new List<Producto>();
        }
    }
}