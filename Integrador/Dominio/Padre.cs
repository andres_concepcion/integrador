﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class Padre : Usuario
    {
        private List<Alumno> lstHijos;

        public List<Alumno> LstHijos
        {
            get { return lstHijos; }
            set { lstHijos = value; }
        }

    }
}