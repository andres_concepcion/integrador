﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public enum Rol
    {
        ADMIN,
        VENDEDOR,
        NUTRICIONISTA,
        PADRE
    }

    public class Usuario
    {
        private int user;
        private string password;
        private Rol rol;

        public int User
        {
            get { return user; }
            set { user = value; }
        }

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public Rol Rol
        {
            get { return rol; }
            set { rol = value; }
        }

    }
}