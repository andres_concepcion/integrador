﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class Alumno
    {
        private int ci;
        private String nombre;
        private List<string> lstAtributosNoPermitidos;

        public int Ci
        {
            get { return ci; }
            set { ci = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public List<string> LstAtributosNoPermitidos
        {
            get { return lstAtributosNoPermitidos; }
            set { lstAtributosNoPermitidos = value; }
        }

        public Alumno()
        {
            lstAtributosNoPermitidos = new List<string>();
        }
    }
}