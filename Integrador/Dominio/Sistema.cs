﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Integrador.Persistencia;

namespace Integrador.Dominio
{
    public class Sistema
    {
        #region Singleton
        private static Sistema instancia;
        public static Sistema getInstancia()
        {
            if (instancia == null)
            {
                instancia = new Sistema();
            }
            return instancia;
        }
        private Sistema() { }
        #endregion

        #region Usuario
        public string iniciarSesion(int pCi, string pPass)
        {
            return ControllerUsuario.getInstancia().inicioSesion(pCi, pPass);
        }
        public Usuario buscarUsuarioXCi(int pCi)
        {
            return ControllerUsuario.getInstancia().buscarUsuario(pCi);
        }
        public bool registroUsuario(Usuario pUsuario)
        {
            return ControllerUsuario.getInstancia().registroUsuario(pUsuario);
        }
        public bool actualizarUsuario(Usuario pUsuario)
        {
            return ControllerUsuario.getInstancia().actualizarUsuario(pUsuario);
        }
        #endregion
        #region Producto
        public List<string> listarAtributos()
        {
            return ControllerProducto.getInstancia().listarAtributos();
        }
        public Producto buscarProductoPorId(int pId)
        {
            return ControllerProducto.getInstancia().buscarProductoPorId(pId);
        }
        public bool altaProducto(Producto pProducto)
        {
            return ControllerProducto.getInstancia().altaProducto(pProducto);
        }
        public bool modificarProducto(Producto pProducto)
        {
            return ControllerProducto.getInstancia().modificarProducto(pProducto);
        }
        public List<string> listarStringProductos()
        {
            List<Producto> lstProductos = ControllerProducto.getInstancia().listarProductos();
            List<string> lstAux = new List<string>();
            foreach (Producto prod in lstProductos)
            {
                lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
            }
            return lstAux;
        }
        public bool agregarAtributo(string pAtributo)
        {
            return ControllerProducto.getInstancia().agregarAtributo(pAtributo);
        }

        public double devolverPrecio(List<Producto> pLstProductos)
        {
            double precio = 0;
            foreach (Producto producto in pLstProductos)
            {
                precio += producto.Precio;
            }
            return precio;
        }

        public List<String> listarAtributosEntreFechas(DateTime pFechaInicio, DateTime pFechaFin)
        {
            return ControllerProducto.getInstancia().listarAtributosEntreFechas(pFechaInicio,pFechaFin);
        }
        #endregion
        #region Menu
        public bool altaMenu(Menu pMenu)
        {
            return ControllerMenu.getInstancia().altaMenu(pMenu);
        }
        public Menu buscarMenuPorFechaYNombre(DateTime pFecha, string pNombre)
        {
            return ControllerMenu.getInstancia().buscarMenuPorFechaYNombre(pFecha, pNombre);
        }
        public bool modificarMenu(Menu pMenu)
        {
            return ControllerMenu.getInstancia().modificarMenu(pMenu);
        }
        public List<string> listarStringProductosMenu(Menu pMenu)
        {
            List<string> lstAux = new List<string>();
            foreach (Producto prod in pMenu.LstProductos)
            {
                lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
            }
            return lstAux;
        }
        public List<string> listarStringMenu(Menu pMenu)
        {
            List<string> lstAux = new List<string>();
            lstAux.Add(pMenu.Nombre);
            foreach (Producto prod in pMenu.LstProductos)
            {
                lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
            }
            return lstAux;
        }

        public List<string> listarStringMenuMismaSemana(DateTime pFecha)
        {
            List<Menu> lstAuxMenu = ControllerMenu.getInstancia().listarMenuesPorFecha(pFecha);
            List<string> lstAux = new List<string>();
            foreach (Menu menu in lstAuxMenu)
            {
                lstAux.Add(menu.Nombre);
                foreach (Producto prod in menu.LstProductos)
                {
                    lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
                }
            }
            return lstAux;
        }

        #endregion
        #region Alumno
        public Alumno buscarAlumnoPorCi(int pCi)
        {
            return ControllerAlumno.getInstancia().buscarAlumnoPorCi(pCi);
        }
        public List<string> listarStringAlumnos(int pCiPadre)
        {
            List<Alumno> lstAlumnos = ControllerAlumno.getInstancia().listarAlumnosPorPadre(pCiPadre);
            List<string> lstAux = new List<string>();
            foreach (Alumno alumno in lstAlumnos)
            {
                lstAux.Add("Ci: " + alumno.Ci + ", Nombre: " + alumno.Nombre);
            }
            return lstAux;
        }
        public List<string> listarStringAtributosAlumnos(Alumno pAlumno)
        {
            List<string> lstAux = new List<string>();
            foreach (string atributo in pAlumno.LstAtributosNoPermitidos)
            {
                lstAux.Add(atributo);
            }
            return lstAux;
        }
        public void guardarAtributosAlumno(Alumno pAlumno)
        {
            ControllerAlumno.getInstancia().guardarAtributosAlumno(pAlumno);
        }
        public List<string> productosPermitidos(Alumno pAlumno)
        {
            List<Producto> lstProductos = ControllerProducto.getInstancia().listarProductos();
            List<string> lstAux = new List<string>();
            for (int i = 0; i < lstProductos.Count - 1; i++)
            {
                foreach (string atributo in pAlumno.LstAtributosNoPermitidos)
                {
                    if (lstProductos[i].LstAtributos.Contains(atributo))
                    {
                        lstProductos.RemoveAt(i);
                        i = -1;
                    }
                }
            }
            foreach (Producto prod in lstProductos)
            {
                lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
            }
            return lstAux;
        }
        public List<string> menusPermitidos(Alumno pAlumno)
        {
            List<Menu> lstAuxMenu = ControllerMenu.getInstancia().listarMenuesPorFecha(DateTime.Now);
            List<string> lstAux = new List<string>();
            foreach (Menu menu in lstAuxMenu)
            {
                List<string> lstAtributos = new List<string>();
                bool auxContiene = false;
                foreach (Producto producto in menu.LstProductos)
                {
                    foreach (string atributo in producto.LstAtributos)
                    {
                        if (!lstAtributos.Contains(atributo))
                        {
                            lstAtributos.Add(atributo);
                        }
                    }
                }
                foreach (string atributo in pAlumno.LstAtributosNoPermitidos)
                {
                    if (lstAtributos.Contains(atributo))
                    {
                        auxContiene = true;
                    }
                }
                if (auxContiene == false)
                {
                    lstAux.Add(menu.Nombre);
                    foreach (Producto prod in menu.LstProductos)
                    {
                        lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
                    }
                }
            }
            return lstAux;
        }
        #endregion
        #region MenuDiario
        public void altaMenuDIario(MenuDiario pMenu)
        {
            ControllerMenuDiario.getInstancia().altaMenuDiario(pMenu);
        }

        public MenuDiario buscarMenuDiarioPorFechaYAlumno(DateTime pFecha, int pCiAlumno)
        {
            return ControllerMenuDiario.getInstancia().buscarMenuDiarioPorFechaYALumno(pFecha, pCiAlumno);
        }
        public List<string> listarStringMenuDiario(MenuDiario pMenu)
        {
            List<string> lstAux = new List<string>();
            foreach (Producto prod in pMenu.LstProductos)
            {
                lstAux.Add("Id: " + prod.Id + ", Descripcion: " + prod.Descripcion);
            }
            return lstAux;
        }
        #endregion
        #region Factura
        public bool altaFactura(Factura pFactura)
        {
            return ControllerFactura.getInstancia().altaFactura(pFactura);
        }
        public double totalFacturasPorFecha(DateTime pFechaInicio, DateTime pFechaFin)
        {
            return ControllerFactura.getInstancia().totalFacturasPorFecha(pFechaInicio, pFechaFin);
        }
        #endregion
    }
}