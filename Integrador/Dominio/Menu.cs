﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class Menu
    {
        private DateTime fecha;
        private String nombre;
        private List<Producto> lstProductos;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public List<Producto> LstProductos
        {
            get { return lstProductos; }
            set { lstProductos = value; }
        }

        public Menu()
        {
            lstProductos = new List<Producto>();
        }
    }
}