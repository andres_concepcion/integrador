﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class MenuDiario
    {
        private DateTime fecha;
        private Alumno alumno;
        private List<Producto> lstProductos;

        public DateTime Fecha
        {
            get { return fecha; }
            set { fecha = value; }
        }
        
        public Alumno Alumno
        {
            get { return alumno; }
            set { alumno = value; }
        }

        public List<Producto> LstProductos
        {
            get { return lstProductos; }
            set { lstProductos = value; }
        }
        public MenuDiario()
        {
            lstProductos = new List<Producto>();
        }
    }
}