﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Integrador.Dominio
{
    public class Producto
    {
        private int id;
        private string descripcion;
        private List<string> lstAtributos;
        private double precio;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public List<string> LstAtributos
        {
            get { return lstAtributos; }
            set { lstAtributos = value; }
        }

        public double Precio
        {
            get { return precio; }
            set { precio = value; }
        }

        public Producto()
        {
            lstAtributos = new List<string>();
        }
    }
}