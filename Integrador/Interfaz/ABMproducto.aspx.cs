﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;

namespace Integrador.Interfaz
{
    public partial class ABMproducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lboxAtributos.DataSource = Sistema.getInstancia().listarAtributos();
                lboxAtributos.DataBind();
                lboxProductos.DataSource = Sistema.getInstancia().listarStringProductos();
                lboxProductos.DataBind();
            }
            if (IsPostBack)
            {
                lboxProductos.Items.Clear();
                lboxProductos.DataSource = Sistema.getInstancia().listarStringProductos();
                lboxProductos.DataBind();
            }
        }

        protected void btnAgregarAtributos_Click(object sender, EventArgs e)
        {
            if (lboxAtributos.SelectedItem != null)
            {
                lboxAtributosProducto.Items.Add(lboxAtributos.SelectedItem);
                lboxAtributos.Items.RemoveAt(lboxAtributos.SelectedIndex);
            }
        }

        protected void btnQuitarAtributos_Click(object sender, EventArgs e)
        {
            if (lboxAtributosProducto.SelectedItem != null)
            {
                lboxAtributos.Items.Add(lboxAtributosProducto.SelectedItem);
                lboxAtributosProducto.Items.Remove(lboxAtributosProducto.SelectedItem);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Producto auxProducto = new Producto();
                auxProducto.Descripcion = txtDescripcion.Text;
                auxProducto.Precio = 0;
                foreach (object item in lboxAtributosProducto.Items)
                {
                    auxProducto.LstAtributos.Add(item.ToString());
                }
                bool aux = Sistema.getInstancia().altaProducto(auxProducto);
                if (aux == true)
                {
                    lblError.Text = "El producto fue ingresado con exito";
                    Response.Redirect("~/Interfaz/ABMproducto.aspx");
                }
                else
                {
                    lblError.Text = "Se produjo un error";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Producto auxProducto = Sistema.getInstancia().buscarProductoPorId(int.Parse(txtId.Text));
                if (auxProducto != null)
                {
                    txtDescripcion.Text = auxProducto.Descripcion;
                    txtPrecio.Text = auxProducto.Precio.ToString();
                    lboxAtributosProducto.DataSource = auxProducto.LstAtributos;
                    lboxAtributosProducto.DataBind();
                }
                else
                {
                    lblError.Text = "El producto no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                Producto auxProducto = Sistema.getInstancia().buscarProductoPorId(int.Parse(txtId.Text));
                if (auxProducto != null)
                {
                    auxProducto.Descripcion = txtDescripcion.Text;
                    auxProducto.Precio = int.Parse(txtPrecio.Text);
                    foreach (object item in lboxAtributosProducto.Items)
                    {
                        auxProducto.LstAtributos.Add(item.ToString());
                    }
                    bool aux = Sistema.getInstancia().modificarProducto(auxProducto);
                    if (aux == true)
                    {
                        lblError.Text = "El producto fue modificado con exito";
                        Response.Redirect("~/Interfaz/ABMproducto.aspx");
                    }
                }
                else
                {
                    lblError.Text = "Se produjo un error";
                }

            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnNuevoAtributo_Click(object sender, EventArgs e)
        {
            bool aux = Sistema.getInstancia().agregarAtributo(txtNuevoAtributo.Text);
            if (aux == true)
            {
                lboxAtributos.Items.Clear();
                lboxAtributos.DataSource = Sistema.getInstancia().listarAtributos();
                lboxAtributos.DataBind();
            }
            else
            {
                lblError.Text = "El atributo ya existe";
            }
        }


    }
}