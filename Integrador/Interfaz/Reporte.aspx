﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reporte.aspx.cs" Inherits="Integrador.Interfaz.Reporte" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <br />
        <h1>
            <asp:Label ID="lblReporte" runat="server" Text="Reporte ventas"></asp:Label></h1>
        
            <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio"></asp:Label>
        
            <asp:Calendar ID="clrInicio" runat="server"></asp:Calendar>
            <asp:Label ID="lblFechaFin" runat="server" Text="Fecha Fin"></asp:Label>
            <asp:Calendar ID="clrFin" runat="server"></asp:Calendar>
       
        <br />
        <asp:Label ID="lblTotal" runat="server" Text="Total ventas"></asp:Label>
&nbsp;
        <asp:TextBox ID="txtTotal" runat="server"></asp:TextBox>
&nbsp;
        <asp:Button ID="btnGenerar" runat="server" onclick="btnGenerar_Click" 
            Text="Generar" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
       
    </div>
    </form>
</body>
</html>
