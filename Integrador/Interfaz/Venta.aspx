﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Venta.aspx.cs" Inherits="Integrador.Interfaz.Venta" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
    
        <br />
           <h1><asp:Label ID="lblVenta" runat="server" Text="Venta"></asp:Label></h1>
        
        <asp:Label ID="lblAlumno" runat="server" Text="Alumno"></asp:Label>
        &nbsp;<asp:TextBox ID="txtAlumno" runat="server"></asp:TextBox>
&nbsp;<asp:Button ID="btnVer" runat="server" OnClick="btnVer_Click" Text="Ver" />
        <br />
        <asp:Label ID="lblProductos" runat="server" Text="Productos"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblAFacturar" runat="server" Text="A Facturar"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:ListBox ID="lboxProductosMenu" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <asp:Button ID="btnQuitarProductos" runat="server" Text="&lt;=" 
                onclick="btnQuitarProductosMenu_Click" />
        <asp:Button ID="btnAgregarProductos" runat="server" Text="=&gt;" 
                onclick="btnAgregarProductosMenu_Click" />
        <asp:ListBox ID="lboxAFacturar" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
       
    
        <br />
        <br />
        <asp:Button ID="btnFacturar" runat="server" onclick="btnFacturar_Click" 
            Text="Facturar" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
       
    
    </div>
    </form>
</body>
</html>
