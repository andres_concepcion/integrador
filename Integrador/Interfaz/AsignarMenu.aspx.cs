﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;
using System.Text.RegularExpressions;

namespace Integrador.Interfaz
{
    public partial class AsignarMenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlAlumnos.DataSource = Sistema.getInstancia().listarStringAlumnos(int.Parse(Session["Usuario"].ToString()));
                ddlAlumnos.DataBind();
            }
        }
        protected void btnAgregarProductosMenu_Click(object sender, EventArgs e)
        {
            if (lboxProductosMenu.SelectedItem != null)
            {
                lboxMenuDiario.Items.Add(lboxProductosMenu.SelectedItem);
                lboxProductosMenu.Items.RemoveAt(lboxProductosMenu.SelectedIndex);
            }
        }
        protected void btnQuitarProductosMenu_Click(object sender, EventArgs e)
        {
            if (lboxMenuDiario.SelectedItem != null)
            {
                lboxProductosMenu.Items.Add(lboxMenuDiario.SelectedItem);
                lboxMenuDiario.Items.RemoveAt(lboxMenuDiario.SelectedIndex);
            }
        }

        protected void btnAgregarProductos_Click(object sender, EventArgs e)
        {
            if (lboxProductos.SelectedItem != null)
            {
                lboxMenuDiario.Items.Add(lboxProductos.SelectedItem);
                lboxProductos.Items.RemoveAt(lboxProductos.SelectedIndex);
            }
        }

        protected void btnQuitarProductos_Click(object sender, EventArgs e)
        {
            if (lboxProductosMenu.SelectedItem != null)
            {
                lboxProductos.Items.Add(lboxMenuDiario.SelectedItem);
                lboxMenuDiario.Items.RemoveAt(lboxMenuDiario.SelectedIndex);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                string auxString = ddlAlumnos.SelectedValue.ToString();
                string ciAlumno = Regex.Match(auxString, @"\d+").Value;
                MenuDiario auxMenu = Sistema.getInstancia().buscarMenuDiarioPorFechaYAlumno(calendario.SelectedDate, int.Parse(ciAlumno));
                if (auxMenu.Alumno == null)
                {
                    auxMenu.Fecha = calendario.SelectedDate;
                    auxMenu.Alumno = Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(ciAlumno));
                    foreach (object item in lboxMenuDiario.Items)
                    {
                        auxString = item.ToString();
                        string resultString = Regex.Match(auxString, @"\d+").Value;
                        auxMenu.LstProductos.Add(Sistema.getInstancia().buscarProductoPorId(int.Parse(resultString)));
                    }
                    Sistema.getInstancia().altaMenuDIario(auxMenu);
                    lblError.Text = "El menu fue ingresado con exito";
                }
                else
                {
                    lblError.Text = "Ya existe un menu asignado a esa fecha";
                }
            }
            catch
            {
                lblError.Text = "Se produjo un error";
            }
        }

        protected void btnVer_Click(object sender, EventArgs e)
        {
            string auxString = ddlAlumnos.SelectedItem.ToString();
            string resultString = Regex.Match(auxString, @"\d+").Value;
            Alumno aux = Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(resultString));
            lboxProductos.DataSource = Sistema.getInstancia().productosPermitidos(aux);
            lboxProductos.DataBind();
            lboxProductosMenu.DataSource = Sistema.getInstancia().menusPermitidos(aux);
            lboxProductosMenu.DataBind();
        }




    }
}