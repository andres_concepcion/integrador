﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Integrador.Interfaz.Home" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Menu ID="menuPrincipal" runat="server" Orientation="Horizontal" 
            StaticSubMenuIndent="16px">
            <Items>
                <asp:MenuItem Text="ABM Usuario" Value="ABMUsuario" 
                    NavigateUrl="~/Interfaz/ABMusuarios.aspx"></asp:MenuItem>
                <asp:MenuItem Text="|" Value="Separador1" Enabled="False"></asp:MenuItem>
                <asp:MenuItem Text="ABM Producto" Value="ABMProducto" 
                    NavigateUrl="~/Interfaz/ABMproducto.aspx"></asp:MenuItem>
                <asp:MenuItem Text="|" Value="Separador2" Enabled="False"></asp:MenuItem>
                <asp:MenuItem Text="ABM Menu" Value="ABMMenu" 
                    NavigateUrl="~/Interfaz/ABMmenu.aspx"></asp:MenuItem>
                <asp:MenuItem Text="|" Value="Separador3" Enabled="False"></asp:MenuItem>
                <asp:MenuItem Text="Asignar Menu" Value="AsignarMenu" 
                    NavigateUrl="~/Interfaz/AsignarMenu.aspx"></asp:MenuItem>
                <asp:MenuItem Enabled="False" Text="|" Value="Separador4"></asp:MenuItem>
                <asp:MenuItem Text="Asignar Atributos" Value="AsignarAtributos" 
                    NavigateUrl="~/Interfaz/AsignarAtributos.aspx"></asp:MenuItem>
                <asp:MenuItem Enabled="False" Text="|" Value="Separador5"></asp:MenuItem>
                <asp:MenuItem Text="Venta" Value="Venta" NavigateUrl="~/Interfaz/Venta.aspx"></asp:MenuItem>
                <asp:MenuItem Enabled="False" Text="|" Value="Separador6"></asp:MenuItem>
                <asp:MenuItem Text="Pago" Value="Pago"></asp:MenuItem>
                <asp:MenuItem Enabled="False" Text="|" Value="Separador7"></asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/Interfaz/Reporte.aspx" Text="Reporte" 
                    Value="Reporte"></asp:MenuItem>
                <asp:MenuItem Text="|" Value="Separador8"></asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/Interfaz/Defensa.aspx" Text="Defensa" 
                    Value="Defensa"></asp:MenuItem>
            </Items>
        </asp:Menu>
        <br />
        <br />
        <br />
        <h1><asp:Label ID="lblHome" runat="server" Text="Bienvenido a La Cantina"></asp:Label></h1>
    </div>
    </form>
</body>
</html>
