﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;

namespace Integrador.Interfaz
{
    public partial class Defensa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                lBoxListado.DataSource = Sistema.getInstancia().listarAtributosEntreFechas(clrInicio.SelectedDate, clrFin.SelectedDate);
                lBoxListado.DataBind();
            }
            catch
            {
                lblError.Text = "Se ha producido un error, vuelva a elegir las fechas";
            }
        }
    }
}