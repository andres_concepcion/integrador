﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;
using System.Text.RegularExpressions;

namespace Integrador.Interfaz
{
    public partial class AsignarAtributos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lboxAtributos.DataSource = Sistema.getInstancia().listarAtributos();
                lboxAtributos.DataBind();
                ddlAlumnos.DataSource = Sistema.getInstancia().listarStringAlumnos(int.Parse(Session["Usuario"].ToString()));
                ddlAlumnos.DataBind();
            }
        }

        protected void btnAgregarAtributos_Click(object sender, EventArgs e)
        {
            if (lboxAtributos.SelectedItem != null)
            {
                lboxAtributosNoPermitidos.Items.Add(lboxAtributos.SelectedItem);
                lboxAtributos.Items.RemoveAt(lboxAtributos.SelectedIndex);
            }
        }

        protected void btnQuitarAtributos_Click(object sender, EventArgs e)
        {
            if (lboxAtributosNoPermitidos.SelectedItem != null)
            {
                lboxAtributos.Items.Add(lboxAtributosNoPermitidos.SelectedItem);
                lboxAtributosNoPermitidos.Items.Remove(lboxAtributosNoPermitidos.SelectedItem);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                string auxString = ddlAlumnos.SelectedItem.ToString();
                string resultString = Regex.Match(auxString, @"\d+").Value;
                Alumno auxAlumno = Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(resultString));
                auxAlumno.LstAtributosNoPermitidos.Clear();
                foreach (object item in lboxAtributosNoPermitidos.Items)
                {
                    auxAlumno.LstAtributosNoPermitidos.Add(item.ToString());
                }
                Sistema.getInstancia().guardarAtributosAlumno(auxAlumno);
            }
            catch
            {
                lblError.Text = "Se ha producido un error";
            }
        }

        protected void btnVerAtributos_Click(object sender, EventArgs e)
        {
            string auxString = ddlAlumnos.SelectedItem.ToString();
            string resultString = Regex.Match(auxString, @"\d+").Value;
            lboxAtributosNoPermitidos.DataSource = Sistema.getInstancia().listarStringAtributosAlumnos(Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(resultString)));
            lboxAtributosNoPermitidos.DataBind();
        }
    }
}