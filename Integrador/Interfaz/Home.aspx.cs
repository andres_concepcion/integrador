﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Integrador.Interfaz
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((string)Session["TipoUsuario"] == "VENDEDOR")
            {
                Response.Redirect("~/Interfaz/Venta.aspx");

            }
            else if ((string)Session["TipoUsuario"] == "NUTRICIONISTA")
            {
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("ABMUsuario"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador3"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("AsignarMenu"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador4"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("AsignarAtributos"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador5"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Venta"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador6"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Pago"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador7"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Reporte"));
                
            }
            else if ((string)Session["TipoUsuario"] == "PADRE")
            {
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("ABMUsuario"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador1"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("ABMProducto"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador2"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("ABMMenu"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador3"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador5"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Venta"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador6"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Pago"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Separador7"));
                menuPrincipal.Items.Remove(menuPrincipal.FindItem("Reporte"));
            }
        }
    }
}