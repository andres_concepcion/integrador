﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;
using System.Text.RegularExpressions;

namespace Integrador.Interfaz
{
    public partial class Venta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnVer_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Alumno aux = Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(txtAlumno.Text));
                if (aux != null)
                {
                    MenuDiario auxMenu = Sistema.getInstancia().buscarMenuDiarioPorFechaYAlumno(DateTime.Now, aux.Ci);
                    if (auxMenu.LstProductos != null)
                    {
                        lboxProductosMenu.DataSource = Sistema.getInstancia().listarStringMenuDiario(auxMenu);
                        lboxProductosMenu.DataBind();
                    }
                    else
                    {
                        lboxProductosMenu.DataSource = Sistema.getInstancia().productosPermitidos(aux);
                        lboxProductosMenu.DataBind();
                    }
                }
                else
                {
                    lblError.Text = "El alumno no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnAgregarProductosMenu_Click(object sender, EventArgs e)
        {
            if (lboxProductosMenu.SelectedItem != null)
            {
                lboxAFacturar.Items.Add(lboxProductosMenu.SelectedItem);
                lboxProductosMenu.Items.RemoveAt(lboxProductosMenu.SelectedIndex);
            }
        }

        protected void btnQuitarProductosMenu_Click(object sender, EventArgs e)
        {
            if (lboxAFacturar.SelectedItem != null)
            {
                lboxProductosMenu.Items.Add(lboxAFacturar.SelectedItem);
                lboxAFacturar.Items.RemoveAt(lboxAFacturar.SelectedIndex);
            }
        }

        protected void btnFacturar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Factura auxFactura = new Factura();
                auxFactura.Fecha = DateTime.Now;
                auxFactura.Alumno = Sistema.getInstancia().buscarAlumnoPorCi(int.Parse(txtAlumno.Text));
                foreach (object item in lboxAFacturar.Items)
                {
                    string auxString = item.ToString();
                    string resultString = Regex.Match(auxString, @"\d+").Value;
                    auxFactura.LstProductos.Add(Sistema.getInstancia().buscarProductoPorId(int.Parse(resultString)));
                }
                auxFactura.Precio = Sistema.getInstancia().devolverPrecio(auxFactura.LstProductos);
                bool aux = Sistema.getInstancia().altaFactura(auxFactura);
                if (aux == true)
                {
                    lblError.Text = "El factura fue ingresada con exito";
                    Response.Redirect("~/Interfaz/Venta.aspx");
                }
                else
                {
                    lblError.Text = "Se produjo un error";
                }

            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }
    }
}