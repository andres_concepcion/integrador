﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ABMproducto.aspx.cs" Inherits="Integrador.Interfaz.ABMproducto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <br />
    
        <h1><asp:Label ID="lblProducto" runat="server" Text="ABM Producto"></asp:Label></h1>
        <br />
        <asp:Label ID="lblId" runat="server" Text="Id"></asp:Label>
        <asp:TextBox ID="txtId" runat="server"></asp:TextBox>
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
            onclick="btnBuscar_Click" />
        <br />
        <asp:Label ID="lblDescripcion" runat="server" Text="Descripcion"></asp:Label>
        <asp:TextBox ID="txtDescripcion" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblPrecio" runat="server" Text="Precio"></asp:Label>
        <asp:TextBox ID="txtPrecio" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblAtributos" runat="server" Text="Atributos"></asp:Label>
        <br />
        <asp:ListBox ID="lboxAtributosProducto" runat="server" Height="189px" 
            SelectionMode="Multiple" style="margin-top: 0px" Width="143px">
        </asp:ListBox>
        <asp:Button ID="btnAgregarAtributos" runat="server" Text="&lt;=" 
            onclick="btnAgregarAtributos_Click" />
        <asp:Button ID="btnQuitarAtributos" runat="server" Text="=&gt;" 
            onclick="btnQuitarAtributos_Click" />
        <asp:ListBox ID="lboxAtributos" runat="server" Height="189px" 
            SelectionMode="Multiple" style="margin-top: 0px" Width="143px">
        </asp:ListBox>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:ListBox ID="lboxProductos" runat="server" Height="189px" Width="366px">
        </asp:ListBox>
        <br />
        <br />
        <asp:Label ID="lblIngreso" runat="server" Text="Ingresar nuevo atributo"></asp:Label>
&nbsp;
        <asp:TextBox ID="txtNuevoAtributo" runat="server" Height="24px"></asp:TextBox>
&nbsp;
        <asp:Button ID="btnNuevoAtributo" runat="server" 
            onclick="btnNuevoAtributo_Click" Text="+" />
        <br />
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" 
            onclick="btnGuardar_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="Modificar" 
            onclick="btnModificar_Click" />
    
        <asp:Label ID="lblError" runat="server"></asp:Label>
    
    </div>
    </form>
</body>
</html>
