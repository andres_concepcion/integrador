﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;
using System.Text.RegularExpressions;

namespace Integrador.Interfaz
{
    public partial class ABMmenu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lboxProductos.DataSource = Sistema.getInstancia().listarStringProductos();
                lboxProductos.DataBind();
            }
        }

        protected void btnAgregarProductos_Click(object sender, EventArgs e)
        {
            if (lboxProductos.SelectedItem != null)
            {
                lboxProductosMenu.Items.Add(lboxProductos.SelectedItem);

                lboxProductos.Items.RemoveAt(lboxProductos.SelectedIndex);
            }
        }

        protected void btnQuitarProductos_Click(object sender, EventArgs e)
        {
            if (lboxProductosMenu.SelectedItem != null)
            {
                lboxProductos.Items.Add(lboxProductosMenu.SelectedItem);

                lboxProductosMenu.Items.RemoveAt(lboxProductosMenu.SelectedIndex);
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Integrador.Dominio.Menu auxMenu = Sistema.getInstancia().buscarMenuPorFechaYNombre(System.DateTime.Now, txtNombre.Text);
                if (auxMenu.Nombre == null)
                {
                    auxMenu.Fecha = calendario.SelectedDate;
                    auxMenu.Nombre = txtNombre.Text;
                    foreach (object item in lboxProductosMenu.Items)
                    {
                        string auxString = item.ToString();
                        string resultString = Regex.Match(auxString, @"\d+").Value;
                        auxMenu.LstProductos.Add(Sistema.getInstancia().buscarProductoPorId(int.Parse(resultString)));
                    }
                    bool aux = Sistema.getInstancia().altaMenu(auxMenu);
                    if (aux == true)
                    {
                        lblError.Text = "El producto fue ingresado con exito";
                        Response.Redirect("~/Interfaz/ABMmenu.aspx");
                    }
                    else
                    {
                        lblError.Text = "Se produjo un error";
                    }
                }
                else
                {
                    lblError.Text = "Ya existe un menu con ese nombre";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Integrador.Dominio.Menu auxMenu = Sistema.getInstancia().buscarMenuPorFechaYNombre(calendario.SelectedDate, txtNombre.Text);
                if (auxMenu != null)
                {
                    lboxProductosMenu.DataSource = Sistema.getInstancia().listarStringProductosMenu(auxMenu);
                    lboxProductosMenu.DataBind();
                }
                else
                {
                    lblError.Text = "El menu no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Integrador.Dominio.Menu auxMenu = Sistema.getInstancia().buscarMenuPorFechaYNombre(calendario.SelectedDate, txtNombre.Text);
                auxMenu.Nombre = txtNombre.Text;
                auxMenu.LstProductos.Clear();
                foreach (object item in lboxProductosMenu.Items)
                {
                    string auxString = item.ToString();
                    string resultString = Regex.Match(auxString, @"\d+").Value;
                    auxMenu.LstProductos.Add(Sistema.getInstancia().buscarProductoPorId(int.Parse(resultString)));
                }
                bool aux = Sistema.getInstancia().modificarMenu(auxMenu);
                if (aux == true)
                {
                    lblError.Text = "El producto fue modificado con exito";
                    Response.Redirect("~/Interfaz/ABMmenu.aspx");
                }
                else
                {
                    lblError.Text = "Se produjo un error";
                }

            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

    }
}