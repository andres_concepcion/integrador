﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;

namespace Integrador.Interfaz
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = "";
        }

        protected void btnIngreso_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Usuario auxUsuario = Sistema.getInstancia().buscarUsuarioXCi(int.Parse(txtCi.Text));
                if (auxUsuario != null)
                {
                    if (txtPassword.Text == auxUsuario.Password)
                    {
                        Session["Usuario"] = auxUsuario.User;
                        Session["TipoUsuario"] = auxUsuario.Rol.ToString();
                        Response.Redirect("~/Interfaz/Home.aspx");
                    }
                    else if (txtPassword.Text != auxUsuario.Password)
                    {
                        lblError.Text = "La contrasenia no es correcta";
                    }
                }
                else if (auxUsuario == null)
                {
                    lblError.Text = "El usuario no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnDefensa_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Interfaz/Defensa.aspx");
        }
    }
}