﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Integrador.Dominio;

namespace Integrador.Interfaz
{
    public partial class ABMusuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Usuario auxUsuario = Sistema.getInstancia().buscarUsuarioXCi(int.Parse(txtCi.Text));
                if (auxUsuario != null)
                {
                    txtPassword.Text = auxUsuario.Password;
                    ddlRol.SelectedValue = auxUsuario.Rol.ToString();
                }
                else if (auxUsuario == null)
                {
                    lblError.Text = "El usuario no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Usuario auxUsuario = Sistema.getInstancia().buscarUsuarioXCi(int.Parse(txtCi.Text));
                if (auxUsuario == null)
                {
                    auxUsuario = new Usuario();
                    auxUsuario.User = int.Parse(txtCi.Text);
                    auxUsuario.Password = txtPassword.Text;
                    auxUsuario.Rol = (Rol)Enum.Parse(typeof(Rol), ddlRol.SelectedValue, true);
                    bool aux = Sistema.getInstancia().registroUsuario(auxUsuario);
                    if (aux == true)
                    {
                        lblError.Text = "El usuario ha sido registrado con exito";
                    }
                    else
                    {
                        lblError.Text = "El usuario no se ha podido registrar";
                    }
                }
                else
                {
                    lblError.Text = "El usuario ya existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                Usuario auxUsuario = Sistema.getInstancia().buscarUsuarioXCi(int.Parse(txtCi.Text));
                if (auxUsuario != null)
                {
                    auxUsuario.Password = txtPassword.Text;
                    auxUsuario.Rol = (Rol)Enum.Parse(typeof(Rol), ddlRol.SelectedValue, true);
                    bool aux = Sistema.getInstancia().actualizarUsuario(auxUsuario);
                    if (aux == true)
                    {
                        lblError.Text = "El usuario ha sido modificado con exito";
                    }
                    else
                    {
                        lblError.Text = "El usuario no se ha podido modificar";
                    }
                }
                else
                {
                    lblError.Text = "El usuario no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }
    }
}