﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ABMusuarios.aspx.cs" Inherits="Integrador.Interfaz.ABMusuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <h1>
            <asp:Label ID="lblTitulo" runat="server" Text="ABM Usuarios"></asp:Label></h1>
        <br />
        <asp:Label ID="lblCedula" runat="server" Text="C.I."></asp:Label>
        <asp:TextBox ID="txtCi" runat="server"></asp:TextBox>
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
            onclick="btnBuscar_Click" />
        <br />
        <asp:Label ID="lblPassword" runat="server" Text="Contraseña"></asp:Label>
        <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblRol" runat="server" Text="Rol"></asp:Label>
        <asp:DropDownList ID="ddlRol" runat="server">
            <asp:ListItem>Seleccionar</asp:ListItem>
            <asp:ListItem Value="VENDEDOR">Vendedor</asp:ListItem>
            <asp:ListItem Value="NUTRICIONISTA">Nutricionista</asp:ListItem>
            <asp:ListItem Value="PADRE">Padre</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" 
            onclick="btnGuardar_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="Modificar" 
            onclick="btnModificar_Click" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
