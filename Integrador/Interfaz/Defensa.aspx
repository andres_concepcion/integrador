﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Defensa.aspx.cs" Inherits="Integrador.Interfaz.Defensa" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>
            <asp:Label ID="lblDefensa" runat="server" Text="Defensa"></asp:Label></h1>
    </div>
        
            <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio"></asp:Label>
        
            <asp:Calendar ID="clrInicio" runat="server"></asp:Calendar>
            <asp:Label ID="lblFechaFin" runat="server" Text="Fecha Fin"></asp:Label>
            <asp:Calendar ID="clrFin" runat="server"></asp:Calendar>
       
        <br />
&nbsp;<asp:Button ID="btnGenerar" runat="server" onclick="btnGenerar_Click" 
            Text="Generar" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
       
    <br />
    <br />
    <asp:ListBox ID="lBoxListado" runat="server" Height="486px" Width="606px">
    </asp:ListBox>
    <br />
    <br />
    </form>
</body>
</html>
