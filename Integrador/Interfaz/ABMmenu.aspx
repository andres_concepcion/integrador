﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ABMmenu.aspx.cs" Inherits="Integrador.Interfaz.ABMmenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <br />
        <h1>
            <asp:Label ID="lblABMmenu" runat="server" Text="ABM Menu"></asp:Label></h1>
        <asp:Label ID="lblNombre" runat="server" Text="Nombre"></asp:Label>
        <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
        &nbsp;<br />
        <asp:Label ID="lblProductos" runat="server" Text="Productos"></asp:Label>
        <br />
        <asp:ListBox ID="lboxProductosMenu" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <asp:Button ID="btnAgregarProductos" runat="server" Text="&lt;=" OnClick="btnAgregarProductos_Click" />
        <asp:Button ID="btnQuitarProductos" runat="server" Text="=&gt;" OnClick="btnQuitarProductos_Click" />
        <asp:ListBox ID="lboxProductos" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <br />
        <asp:Calendar ID="calendario" runat="server" ShowGridLines="True"></asp:Calendar>
        <asp:Label ID="lblBuscar" runat="server" Text="Buscar por fecha y nombre"></asp:Label>
        &nbsp;&nbsp;
        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" 
            onclick="btnBuscar_Click" />
        <br />
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        <asp:Button ID="btnModificar" runat="server" Text="Modificar" 
            onclick="btnModificar_Click" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
