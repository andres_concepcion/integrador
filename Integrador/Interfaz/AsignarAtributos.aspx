﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AsignarAtributos.aspx.cs"
    Inherits="Integrador.Interfaz.AsignarAtributos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <br />
        <h1>
            <asp:Label ID="lblAtributo" runat="server" Text="Asignar atributos no permitidos"></asp:Label></h1>
        <br />
        <asp:Label ID="lblAlumno" runat="server" Text="Alumno"></asp:Label>
        <asp:DropDownList ID="ddlAlumnos" runat="server">
        </asp:DropDownList>
        &nbsp;
        <asp:Button ID="btnVerAtributos" runat="server" OnClick="btnVerAtributos_Click" Text="Ver" />
        <br />
        <asp:Label ID="lblAtributos" runat="server" Text="Atributos"></asp:Label>
        <br />
        <asp:ListBox ID="lboxAtributosNoPermitidos" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="143px"></asp:ListBox>
        <asp:Button ID="btnAgregarAtributos" runat="server" Text="&lt;=" OnClick="btnAgregarAtributos_Click" />
        <asp:Button ID="btnQuitarAtributos" runat="server" Text="=&gt;" OnClick="btnQuitarAtributos_Click" />
        <asp:ListBox ID="lboxAtributos" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="143px"></asp:ListBox>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
