﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Integrador.Interfaz.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>
            <asp:Label ID="lblLogin" runat="server" Text="Login"></asp:Label></h1>
        <br />
        <asp:Label ID="lblCi" runat="server" Text="C.I."></asp:Label>
        <asp:TextBox ID="txtCi" runat="server"></asp:TextBox>
        <br />
        <asp:Label ID="lblPassword" runat="server" Text="Contraseña"></asp:Label>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <asp:Button ID="btnIngreso" runat="server" Text="Ingresar" OnClick="btnIngreso_Click" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btnDefensa" runat="server" onclick="btnDefensa_Click" 
            Text="Defensa" />
    </div>
    </form>
</body>
</html>
