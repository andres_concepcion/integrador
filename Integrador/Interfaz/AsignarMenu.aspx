﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AsignarMenu.aspx.cs" Inherits="Integrador.Interfaz.AsignarMenu" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/Interfaz/Home.aspx">[HOME]</asp:HyperLink>
        <br />
        <h1>
            <asp:Label ID="lblAsignarMenu" runat="server" Text="Asignar Menu"></asp:Label></h1>
        <asp:Label ID="lblAlumno" runat="server" Text="Alumno"></asp:Label>
        <asp:DropDownList ID="ddlAlumnos" runat="server">
        </asp:DropDownList>
        &nbsp;<asp:Button ID="btnVer" runat="server" OnClick="btnVer_Click" Text="Ver" />
        <br />
        <br />
        <asp:Label ID="lblProductosMenu" runat="server" Text="Productos Menu"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblMenuDiario" runat="server" Text="Menu Diario"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblProductos" runat="server" Text="Productos"></asp:Label>
        <br />
        <asp:ListBox ID="lboxProductosMenu" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <asp:Button ID="btnQuitarProductosMenu" runat="server" Text="&lt;=" 
            onclick="btnQuitarProductosMenu_Click" />
        <asp:Button ID="btnAgregarProductosMenu" runat="server" Text="=&gt;" 
            onclick="btnAgregarProductosMenu_Click" />
        <asp:ListBox ID="lboxMenuDiario" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <asp:Button ID="btnAgregarProductos" runat="server" Text="&lt;=" OnClick="btnAgregarProductos_Click" />
        <asp:Button ID="btnQuitarProductos" runat="server" Text="=&gt;" OnClick="btnQuitarProductos_Click" />
        <asp:ListBox ID="lboxProductos" runat="server" Height="189px" SelectionMode="Multiple"
            Style="margin-top: 0px" Width="240px"></asp:ListBox>
        <br />
        <asp:Calendar ID="calendario" runat="server" ShowGridLines="True"></asp:Calendar>
        <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
