﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerAlumno : Persistir
    {
        #region Singleton
        private static ControllerAlumno instancia;
        public static ControllerAlumno getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerAlumno();
            }
            return instancia;
        }
        private ControllerAlumno() { }
        #endregion

        public void guardarAtributosAlumno(Alumno pAlumno)
        {
            try
            {
                borrarAtributosNoPermitidos(pAlumno.Ci);
                foreach (string atributo in pAlumno.LstAtributosNoPermitidos)
                {
                    atributosNoPermitidos(pAlumno.Ci, atributo);
                }
            }
            catch
            {
                throw;
            }
        }

        public Alumno buscarAlumnoPorCi(int pCi)
        {
            try
            {
                Alumno aux = new Alumno();
                string sql = "Select * from Alumnos where Ci = " + pCi;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Ci = pCi;
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.LstAtributosNoPermitidos = listarAtributosPorAlumno(pCi);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Alumno> listarAlumnosPorPadre(int pCi)
        {
            try
            {
                List<Alumno> lstAux = new List<Alumno>();
                Alumno aux;
                string sql = "Select al.Ci from Alumnos as al, PadresAlumnos as pa where al.Ci = pa.CiAlumno and pa.CiPadre = " + pCi;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux = new Alumno();
                    aux = buscarAlumnoPorCi((int)unDr.GetValue(0));
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public bool atributosNoPermitidos(int pCi, string pAtributo)
        {
            try
            {
                bool resultado = false;
                int i = 0;
                string sql = "Insert into AtributosNoPermitidos (CiAlumno, Atributo) values(" + pCi + ", '" + pAtributo + "')";
                i = ejecutarNonQuery(sql);
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void borrarAtributosNoPermitidos(int pCi)
        {
            try
            {
                string sql = "Delete from AtributosNoPermitidos where CiAlumno = " + pCi;
                int i = ejecutarNonQuery(sql);
            }
            catch
            {
                throw;
            }
        }

        public List<string> listarAtributosPorAlumno(int pCi)
        {
            try
            {
                List<string> lstAux = new List<string>();
                string sql = "Select Atributo from AtributosNoPermitidos where CiAlumno = " + pCi;
                SqlDataReader unDr = ejecutarReader(sql);
                string aux = null;
                while (unDr.Read())
                {
                    aux = (string)unDr.GetValue(0);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}