﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerProducto : Persistir
    {
        #region Singleton
        private static ControllerProducto instancia;
        public static ControllerProducto getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerProducto();
            }
            return instancia;
        }
        private ControllerProducto() { }
        #endregion

        #region Producto
        public bool altaProducto(Producto pProducto)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Productos (Descripcion, Precio) values('" + pProducto.Descripcion + "', " + pProducto.Precio + ")";
                int i = ejecutarNonQuery(sql);
                sql = "SELECT IDENT_CURRENT('Productos')";
                int idProducto = int.Parse(ejecutarScalar(sql).ToString());
                foreach (string Atributo in pProducto.LstAtributos)
                {
                    atributosProducto(idProducto, Atributo);
                }
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarProducto(Producto pProducto)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Productos set Descripcion = '" + pProducto.Descripcion + "', Precio = " + pProducto.Precio + " where Id = " + pProducto.Id;
                int i = ejecutarNonQuery(sql);
                borrarAtributosProducto(pProducto.Id);
                foreach (string Atributo in pProducto.LstAtributos)
                {
                    atributosProducto(pProducto.Id, Atributo);
                }
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Producto> listarProductos()
        {
            try
            {
                List<Producto> lstAux = new List<Producto>();
                string sql = "Select * from Productos";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    Producto aux = new Producto();
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Descripcion = (string)unDr.GetValue(1);
                    List<string> lstAtributos = listarAtributosProducto(aux.Id);
                    if (lstAux != null)
                    {
                        aux.LstAtributos = lstAtributos;
                    }
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<Producto> listarProductosPorFactura(int pIdFactura)
        {
            try
            {
                List<Producto> lstAux = new List<Producto>();
                string sql = "Select * from ProductosFactura where IdFactura = " + pIdFactura;
                SqlDataReader unDr = ejecutarReader(sql);
                Producto aux = new Producto();
                while (unDr.Read())
                {
                    aux = buscarProductoPorId((int)unDr.GetValue(1));
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public List<Producto> listarProductosPorMenu(DateTime pFecha, string pNombre)
        {
            try
            {
                List<Producto> lstAux = new List<Producto>();
                string fechaString = pFecha.ToString("MM/dd/yyyy");
                string sql = "Select IdProducto from Integrador.dbo.ProductosMenu where Fecha = '" + fechaString + "' and Nombre = '" + pNombre + "'";
                SqlDataReader unDr = ejecutarReader(sql);
                Producto aux;
                while (unDr.Read())
                {
                    aux = new Producto();
                    aux = buscarProductoPorId((int)unDr.GetValue(0));
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Producto buscarProductoPorId(int pId)
        {
            try
            {
                Producto aux = new Producto();
                string sql = "Select * from Productos where Id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Descripcion = (string)unDr.GetValue(1);
                    List<string> lstAux = listarAtributosProducto(pId);
                    if (lstAux != null)
                    {
                        aux.LstAtributos = lstAux;
                    }
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region AtributosProducto
        public void atributosProducto(int pIdProducto, string pAtributo)
        {
            try
            {
                string sql = "Insert into AtributosProducto (IdProducto, Atributo) values(" + pIdProducto + ", '" + pAtributo + "')";
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void borrarAtributosProducto(int pId)
        {
            try
            {
                string sql = "Delete from AtributosProducto where IdProducto = " + pId;
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<string> listarAtributosProducto(int pIdProducto)
        {
            try
            {
                List<string> lstAux = new List<string>();
                string sql = "Select Atributo from AtributosProducto where IdProducto = " + pIdProducto;
                SqlDataReader unDr = ejecutarReader(sql);
                string aux;
                while (unDr.Read())
                {
                    aux = (string)unDr.GetValue(0);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<string> listarAtributos()
        {
            try
            {
                List<string> lstAux = new List<string>();
                string sql = "Select * from Atributos";
                SqlDataReader unDr = ejecutarReader(sql);
                string aux = null;
                while (unDr.Read())
                {
                    aux = (string)unDr.GetValue(0);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool agregarAtributo(string pAtributo)
        {
            try
            {
                bool aux = false;
                string sql = "Select * from Atributos where Atributo = '" + pAtributo.ToUpper() + "'";
                SqlDataReader unDr = ejecutarReader(sql);
                string auxString = null;
                while (unDr.Read())
                {
                    auxString = (string)unDr.GetValue(0);
                }
                if (auxString == null)
                {
                    sql = "Insert into Atributos (Atributo) values ('" + pAtributo.ToUpper() + "')";
                    int i = ejecutarNonQuery(sql);
                    return aux = true;
                }
                else
                {
                    return aux;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<String> listarAtributosEntreFechas(DateTime pFechaInicio, DateTime pFechaFin)
        {
            try
            {
                String linea = "";
                String atributo = "";
                int total = 0;
                int cantidad = 0;
                double porcentaje = 0;
                List<String> lstResultado = new List<String>();
                String fechaInicioString = pFechaInicio.ToString("yyyy/MM/dd");
                String fechaFinString = pFechaFin.ToString("yyyy/MM/dd");
                string sql = "Select ap.Atributo as atributo, COUNT(ap.Atributo) as cantidad from AtributosProducto as ap, Productos as p, ProductosFactura as pf, Facturas as f where ap.IdProducto = p.Id and p.Id = pf.IdProducto and pf.IdFactura = f.Id and f.Fecha between '" + fechaInicioString + "' and '" + fechaFinString + "' group by Atributo;";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    total += (int)unDr.GetValue(1);
                }
                unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    atributo = (string)unDr.GetValue(0);
                    cantidad = (int)unDr.GetValue(1);
                    porcentaje = (cantidad * 100) / total;
                    linea = "Atributo: " + atributo + ", Cantidad: " + cantidad + ", Porcentaje: " + porcentaje + "%";
                    lstResultado.Add(linea);
                }
                return lstResultado;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}