﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;
using System.Globalization;

namespace Integrador.Persistencia
{
    public class ControllerMenu : Persistir
    {
        #region Singleton
        private static ControllerMenu instancia;
        public static ControllerMenu getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerMenu();
            }
            return instancia;
        }
        private ControllerMenu() { }
        #endregion

        #region Menu
        public bool altaMenu(Menu pMenu)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Menu (Fecha, Nombre) values('" + pMenu.Fecha.ToShortDateString() + "', '" + pMenu.Nombre + "')";
                int i = ejecutarNonQuery(sql);
                foreach (Producto Producto in pMenu.LstProductos)
                {
                    productosMenu(pMenu.Fecha, pMenu.Nombre, Producto.Id);
                }
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarMenu(Menu pMenu)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Menu set Nombre = '" + pMenu.Nombre + "' where Fecha = " + pMenu.Fecha + "and Nombre = " + pMenu.Nombre;
                int i = ejecutarNonQuery(sql);
                borrarProductosMenu(pMenu);
                foreach (Producto Producto in pMenu.LstProductos)
                {
                    productosMenu(pMenu.Fecha, pMenu.Nombre, Producto.Id);
                }
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public List<Menu> listarMenuesPorFecha(DateTime pFecha)
        {
            try
            {
                DayOfWeek primerDiaSemana = DayOfWeek.Monday;
                DayOfWeek ultimoDiaSemana = DayOfWeek.Friday;
                List<Menu> lstAux = new List<Menu>();
                string sql = "Select * from Menu";
                SqlDataReader unDr = ejecutarReader(sql);
                Menu aux;
                while (unDr.Read())
                {
                    aux = new Menu();
                    aux.Fecha = (DateTime)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorMenu(aux.Fecha, aux.Nombre);
                    string fechaString = aux.Fecha.ToString("MM/dd/yyyy");
                    DateTime fecha = DateTime.Parse(fechaString);
                    if (fecha.DayOfWeek >= primerDiaSemana && fecha.DayOfWeek <= ultimoDiaSemana)
                    {
                        aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorMenu(aux.Fecha, aux.Nombre);
                        lstAux.Add(aux);
                    }
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public Menu buscarMenuPorFechaYNombre(DateTime pFecha, string pNombre)
        {
            try
            {
                Menu aux = new Menu();
                string sql = "Select * from Menu where Fecha = '" + pFecha.ToShortDateString() + "' and Nombre = '" + pNombre + "'";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Fecha = pFecha;
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorMenu(aux.Fecha, aux.Nombre);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool productosMenu(DateTime pFecha, string pNombre, int IdProducto)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into ProductosMenu (Fecha, Nombre, IdProducto) values('" + pFecha.ToShortDateString() + "', '" + pNombre + "', " + IdProducto + ")";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public void borrarProductosMenu(Menu pMenu)
        {
            try
            {
                string sql = "Delete from ProductosMenu where Fecha = '" + pMenu.Fecha.ToShortDateString() + "' and Nombre = '" + pMenu.Nombre + "'";
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}