using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerUsuario : Persistir
    {
        #region Singleton
        private static ControllerUsuario instancia;
        public static ControllerUsuario getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerUsuario();
            }
            return instancia;
        }
        private ControllerUsuario() { }
        #endregion
        public string inicioSesion(int pUsuario, string pPassword)
        {
            try
            {
                string sql = "Select Rol from Usuarios where Usuario = " + pUsuario + " and Password = " + pPassword;
                object aux = ejecutarScalar(sql);
                string auxRol = "";
                if (aux != null)
                {
                    auxRol = (string)aux;
                }
                return auxRol;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool registroUsuario(Usuario pUsuario)
        {
            bool aux = false;
            try
            {
                string sql = "Insert into Usuarios (Usuario, Password, Rol) values(" + pUsuario.User + ", '" + pUsuario.Password + "', '" + pUsuario.Rol + "');";
                int i = ejecutarNonQuery(sql);
                if (i == 1)
                {
                    aux = true;
                }
                return aux;
            }
            catch
            {
                return aux;
            }
        }
        public bool actualizarUsuario(Usuario pUsuario)
        {
            bool aux = false;
            try
            {
                string sql = "Update Usuarios set Password = " + pUsuario.Password + ", Rol = '" + pUsuario.Rol + "' where Usuario = " + pUsuario.User;
                int i = ejecutarNonQuery(sql);
                if (i == 1)
                {
                    aux = true;
                }
                return aux;
            }
            catch (Exception)
            {
                return aux;
            }
        }

        public Usuario buscarUsuario(int pUsuario)
        {
            try
            {
                Usuario aux = null;
                string sql = "Select * from Usuarios where Usuario = " + pUsuario;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux = new Usuario();
                    aux.User = (int)unDr.GetValue(0);
                    aux.Password = (string)unDr.GetValue(1);
                    aux.Rol = (Rol)Enum.Parse(typeof(Rol), (string)unDr.GetValue(2), true);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}