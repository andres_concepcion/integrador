using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerPadre : Persistir
    {
        #region Singleton
        private static ControllerPadre instancia;
        public static ControllerPadre getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerPadre();
            }
            return instancia;
        }
        private ControllerPadre() { }
        #endregion

        #region Padre


        public void registrarHijos(Padre pPadre)
        {
            foreach (Alumno hijo in pPadre.LstHijos)
            {
                string sql = "Insert into PadresAlumno (CiPadre, CiAlumno) values (" + pPadre.User + ", " + hijo.Ci + ")";
                int i = ejecutarNonQuery(sql);
            }
        }

        public List<Alumno> listarHijos(int pCiPadre)
        {
            try
            {
                List<Alumno> lstAux = new List<Alumno>();
                string sql = "Select CiAlumno from PadresAlumnos where CiPadre = " + pCiPadre;
                SqlDataReader unDr = ejecutarReader(sql);
                Alumno aux = new Alumno();
                while (unDr.Read())
                {
                    aux = ControllerAlumno.getInstancia().buscarAlumnoPorCi((int)unDr.GetValue(0));
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}

