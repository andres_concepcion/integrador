﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;

namespace Integrador.Persistencia
{
    public class Persistir
    {
        private string direccionArchivo()
        {
            return HttpContext.Current.Server.MapPath(@"~/");

        }
        public SqlConnection conectar()
        {
            try
            {
                XDocument xDoc = XDocument.Load(direccionArchivo() + "Web.config");
                XElement xElem = xDoc.Root.Elements().Single(elem => (string)elem.Name.ToString() == "connectionStrings");
                XElement xHijo = xElem.Elements().Single(elem => (string)elem.Attribute("name") == "Conexion");
                string s = (string)xHijo.Attribute("connectionString").Value;
                SqlConnection con = new SqlConnection(s);
                return con;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ejecutarNonQuery(string sql)
        {
            try
            {
                int res;
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                res = cmd.ExecuteNonQuery();
                con.Close();
                return res;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public object ejecutarScalar(string sql)
        {
            try
            {
                object res = null;
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                res = cmd.ExecuteScalar();
                con.Close();
                return res;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public SqlDataReader ejecutarReader(string sql)
        {
            try
            {
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader unDr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return unDr;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public int devolverBit(bool b)
        {
            int res = 0;
            if (b == true)
            {
                res = 1;
            }
            return res;
        }

    }
}