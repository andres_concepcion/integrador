﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerFactura : Persistir
    {
        #region Singleton
        private static ControllerFactura instancia;
        public static ControllerFactura getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerFactura();
            }
            return instancia;
        }
        private ControllerFactura() { }
        #endregion

        #region Factura
        public bool altaFactura(Factura pFactura)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Facturas (Fecha, Alumno, Precio) values('" + pFactura.Fecha.ToShortDateString() + "', " + pFactura.Alumno.Ci + ", " + pFactura.Precio + ")";
                int i = ejecutarNonQuery(sql);
                sql = "SELECT IDENT_CURRENT('Facturas')";
                int idFactura = int.Parse(ejecutarScalar(sql).ToString());
                foreach (Producto Producto in pFactura.LstProductos)
                {
                    productosFactura(idFactura, Producto.Id);
                }
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool modificarFactura(Factura pFactura)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Facturas set Fecha = '" + pFactura.Fecha.ToShortDateString() + "', Alumno = " + pFactura.Alumno + ", Precio = " + pFactura.Precio + " where Id = " + pFactura.Id;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Factura> listarFacturas()
        {
            try
            {
                List<Factura> lstAux = new List<Factura>();
                string sql = "Select * from Facturas";
                SqlDataReader unDr = ejecutarReader(sql);
                Factura aux = new Factura();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Fecha = (DateTime)unDr.GetValue(1);
                    aux.Alumno = ControllerAlumno.getInstancia().buscarAlumnoPorCi((int)unDr.GetValue(2));
                    aux.Precio = (double)unDr.GetValue(3);
                    aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorFactura(aux.Id);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<Factura> listarFacturasPorAlumno(int pCiAlumno)
        {
            try
            {
                List<Factura> lstAux = new List<Factura>();
                string sql = "Select * from Facturas where Alumno = " + pCiAlumno;
                SqlDataReader unDr = ejecutarReader(sql);
                Factura aux = new Factura();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Fecha = (DateTime)unDr.GetValue(1);
                    aux.Alumno = ControllerAlumno.getInstancia().buscarAlumnoPorCi((int)unDr.GetValue(2));
                    aux.Precio = (double)unDr.GetValue(3);
                    aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorFactura(aux.Id);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Factura buscarFacturaPorId(int pId)
        {
            try
            {
                Factura aux = new Factura();
                string sql = "Select * from Facturas where Id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Fecha = (DateTime)unDr.GetValue(1);
                    aux.Alumno = ControllerAlumno.getInstancia().buscarAlumnoPorCi((int)unDr.GetValue(2));
                    aux.Precio = (double)unDr.GetValue(3);
                    aux.LstProductos = ControllerProducto.getInstancia().listarProductosPorFactura(aux.Id);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool productosFactura(int idFactura, int IdProducto)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into ProductosFactura (IdFactura, IdProducto) values(" + idFactura + ", " + IdProducto + ")";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                {
                    resultado = true;
                }
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public double totalFacturasPorFecha(DateTime pFechaInicio, DateTime pFechaFin)
        {
            try
            {
                double total = 0;
                string sql = "Select Precio from Facturas where Fecha between '" + pFechaInicio.ToShortDateString() + "' and '" + pFechaFin.ToShortDateString() + "'";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    total += (double)unDr.GetValue(0);
                }
                return total;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion
    }
}