﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Integrador.Dominio;

namespace Integrador.Persistencia
{
    public class ControllerMenuDiario : Persistir
    {
        #region Singleton
        private static ControllerMenuDiario instancia;
        public static ControllerMenuDiario getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerMenuDiario();
            }
            return instancia;
        }
        private ControllerMenuDiario() { }
        #endregion

        public void altaMenuDiario(MenuDiario pMenu)
        {
            try
            {
                foreach (Producto Producto in pMenu.LstProductos)
                {
                    string sql = "Insert into MenuDiario (Fecha, CiAlumno, IdProducto) values('" + pMenu.Fecha.ToShortDateString() + "', " + pMenu.Alumno.Ci + "," + Producto.Id + ")";
                    int i = ejecutarNonQuery(sql);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public MenuDiario buscarMenuDiarioPorFechaYALumno(DateTime pFecha, int pCiAlumno)
        {
            try
            {
                MenuDiario aux = new MenuDiario();
                //string fechaString = pFecha.ToString("MM/dd/yyyy");
                string sql = "Select * from MenuDiario where Fecha = '" + pFecha.ToShortDateString() + "' and CiAlumno = " + pCiAlumno;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Fecha = pFecha;
                    aux.Alumno = ControllerAlumno.getInstancia().buscarAlumnoPorCi(pCiAlumno);
                    aux.LstProductos = buscarProductosMenuDiarioPorFechaYALumno(pFecha, pCiAlumno);
                }

                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<Producto> buscarProductosMenuDiarioPorFechaYALumno(DateTime pFecha, int pCiAlumno)
        {
            try
            {
                List<Producto> lstAux = new List<Producto>();
                Producto auxProducto;
                string sql = "Select IdProducto from MenuDiario where Fecha = '" + pFecha.ToShortDateString() + "' and CiAlumno = " + pCiAlumno;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    auxProducto = new Producto();
                    auxProducto = ControllerProducto.getInstancia().buscarProductoPorId((int)unDr.GetValue(0));
                    lstAux.Add(auxProducto);
                }

                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}